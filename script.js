class Calculator {
    constructor(previousOperandTextElement, currentOperandTextElement){
        this.previousOperandTextElement = previousOperandTextElement
        this.currentOperandTextElement = currentOperandTextElement
        this.clear()
    }

    //Clear function to clear current and previous entries
    clear () {
        this.currentOperand = ''
        this.previousOperand = ''
        this.operation = undefined
    }

    //Deleting by slicing off the most recently added number
    delete() {
        this.currentOperand = this.currentOperand.toString().slice(0, -1)
    }

    //To prevent multiple . to be added, this stops user from adding additional .
    appendNumber(number) {
        if (number == '.' && this.currentOperand.includes('.')) return
        this.currentOperand = this.currentOperand.toString() + number.toString()
    }

    
    chooseOperation(operation) {
      if (this.currentOperand === '') return
      //if previously entered value + operation is not null, call function compute()
      if (this.previousOperand != '') {
          this.compute()
      }
      this.operation = operation
      this.previousOperand = this.currentOperand  
      this.currentOperand = ''
      
    }

    //Logic behind calculations
    compute() {
        let computation 
        const prev = parseFloat(this.previousOperand)
        const current = parseFloat(this.currentOperand)
        //if previous or current entries are not numbers do nothing
        if (isNaN(prev) || isNaN(current)) return
        //switch cases to determine which operation to perform
        switch (this.operation) {
            case '+':
                computation = prev + current
                break
            case '-':
                computation = prev - current
                break
            case '*':
                computation = prev * current
                break
            case '÷':
                computation = prev / current
                break
                default:
                    return
        }
        //computing and setting previous operand to empty
        this.currentOperand = computation
        this.operation = undefined
        this.previousOperand = ''

    }

    //Displaying numbers
    getDisplayNumber(number){
        const stringNumber = number.toString()
        //Turning String number into an Array
        const integerDigits = parseFloat(stringNumber.split('.')[0])
        const decimalDigits = stringNumber.split('.')[1]
        let integerDisplay

        //If nothing is entered, keep display as empty
        if(isNaN(integerDigits)) {
            integerDisplay = ''
        } else { //if integer val is added, convert to 'en'   
            integerDisplay = integerDigits.toLocaleString('en', {
                maximumFractionDigits: 0 })    ////no decimal places after this
        }
        if (decimalDigits != null) { //user entered . and numbers after
            return `${integerDisplay}.${decimalDigits}` //concatenating number before and after the . used
        } else { 
            return integerDisplay
        }
    }

    //Update display of calc
    updateDisplay() {
        this.currentOperandTextElement.innerText = this.getDisplayNumber(this.currentOperand)
        if (this.operation != null) {
            this.previousOperandTextElement.innerText = 
                `${this.getDisplayNumber(this.previousOperand)} ${this.operation}`
        } else {
            this.previousOperandTextElement.innerText = ''
        }
    }
}

//cons variables (all different buttons)
//query selectorAll
//query selector
const numberButtons = document.querySelectorAll('[data-number]')
const operationButtons = document.querySelectorAll('[data-operation]')
const equalsButton = document.querySelector('[data-equals]')
const deleteButton = document.querySelector('[data-delete]')
const allClearButton = document.querySelector('[data-all-clear]')
const previousOperandTextElement = document.querySelector('[data-previous-operand]')
const currentOperandTextElement = document.querySelector('[data-current-operand]')

//New calc object with 
const calculator = new Calculator(previousOperandTextElement, currentOperandTextElement)


//Event listeners below for each button
numberButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.appendNumber(button.innerText)
        calculator.updateDisplay()
    })
})

operationButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.chooseOperation(button.innerText)
        calculator.updateDisplay()
    })
})

equalsButton.addEventListener('click', button => {
    calculator.compute()
    calculator.updateDisplay()
})

allClearButton.addEventListener('click', button => {
    calculator.clear()
    calculator.updateDisplay()
})


deleteButton.addEventListener('click', button => {
    calculator.delete()
    calculator.updateDisplay()
})